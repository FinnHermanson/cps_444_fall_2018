#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

int main(int argc, char** argv){
	pid_t child;
	int error, fd[2], i, proc_num;
	
	unsigned long long x1, x2, xn=0;

	//Error trap at command line input
	if ((argc != 2) || ((proc_num = atoi(argv[1])) <= 0)){
		fprintf(stderr, "Improper Usage\nUsage: %s proc_num\n", argv[0]);
		return 1;
	}
	pipe(fd);
	dup2(fd[0], STDIN_FILENO);
	dup2(fd[1], STDOUT_FILENO);

	close(fd[0]);
	close(fd[1]);

	for (i = 1; i < proc_num; i++){
		pipe(fd);
		child = fork();
		if(child > 0){
			dup2(fd[1], STDOUT_FILENO);
		}
		else {
			dup2(fd[0], STDIN_FILENO);
		}
		close(fd[0]);
		close(fd[1]);
		
		if (child)
			break;
	}
	
	if(i==1) {
		printf("1 1");
		printf("%d", EOF);
		fflush(stdout);
		scanf("%llu%llu", &x1, &x2);
		xn = x1 + x2;
	}
	else {
		scanf("%llu%llu", &x1, &x2);
		xn = x1 + x2;
		if(xn < x2){
			fprintf(stderr, "Overflow in process %d\n", i);
			printf("0 0");
		}
		else printf("%llu %llu\n", x2, xn);
	}
	fprintf(stderr, "This is process %d with ID %ld and parent id %ld received %llu %llu and sent %llu\n",
					 i, (long)getpid(), (long)getpid(), x1, x2, xn);
	return 0;
}
