%%

%%

/* called by flex when EOF reached */
int yywrap (void) {
   /* convention is to return 1 */
   return 1;
}

int main (void) {
   /* main entry point for flex */
   yylex();
   return 0;
}
