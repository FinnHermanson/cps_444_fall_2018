/* cat5.l (cat -n) */

%option yylineno

%{
int cc = 0;
%}

%%
^.*\n { cc += strlen(yytext);
        printf("%4d\t%s", yylineno-1, yytext); }
%%

int yywrap (void) {
   return 1;
}

int main (int argc, char** argv) {
   yyin = fopen (argv[1], "r");
   yylex();
   printf ("%d characters.\n", cc);
   fclose (yyin);
   return 0;
}
