/* cat2.l */

%%

. ECHO;

\n ECHO;

"//".*                              {printf ("I found a Single Line Comment");}

[/][*][^*]*[*]+([^*/][^*]*[*]+)*[/] {printf ("I found a Multi-Line Comment");}

%%

int yywrap (void) {
   return 1;
}

int main (int argc, char ** argv) {
   //Single line comment
   

   /*

   multi-line comment

   */

   printf (":%s:\n", argv[1]);
   if ((yyin = fopen(argv[1] , "r")) == NULL) 
      printf ("broken\n");
   if (yyin == stdin)
      printf ("here\n");
   else
      printf ("there\n") ;

   yylex();
   fclose (yyin);
   return 0;
}
