/* cat1.l */

%%
. /* match any character except newline */ printf ("%s", yytext);

\n /* match newline */ printf ("\n");

%%
int yywrap (void) { 
   return 1;
}

int main (void) { 
   yylex();
   return 0;
}
