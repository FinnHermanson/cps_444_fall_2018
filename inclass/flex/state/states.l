%{
%}

%x ONE
%x TWO

%%

a {BEGIN ONE; printf("in ZERO; read a; goto ONE\n"); }

b {BEGIN TWO; printf("in ZERO; read b; goto TWO\n"); }

<TWO>a { printf ("in TWO; read a; goto 0\n"); BEGIN 0; }
<TWO>b { printf ("in TWO; read b; goto 0\n"); BEGIN 0; }
<ONE>a { printf ("in ONE; read a; goto 0\n"); BEGIN 0; }
<ONE>b { printf ("in ONE; read b; goto 0\n"); BEGIN 0; }

. {}
\n {}
<ONE>. {}
<ONE>\n {}
<TWO>. {}
<TWO>\n {}

%%

int yywrap() {
   return 1;
}

int main() {
   yylex();
   return 0;
}
