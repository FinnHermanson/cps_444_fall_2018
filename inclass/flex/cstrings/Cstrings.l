%{
extern int yy_flex_debug;
char buf[100];
char* s = NULL;
%}

%x INQUOTE

%%
\"            { BEGIN INQUOTE; s = buf; }

<INQUOTE>\\\" { *s++ = '\"'; fprintf(stderr, "found escaped quote\n"); }
<INQUOTE>\\\n { fprintf(stderr, "found escaped newline\n"); }
<INQUOTE>\\n  { *s++ = '\n'; fprintf(stderr, "found newline\n"); }
<INQUOTE>\\t  { *s++ = '\t'; fprintf(stderr, "found tab\n"); }

<INQUOTE>["]  { *s = '\0';
                BEGIN 0;
                printf ("\nFound :%s:\n", buf); }

<INQUOTE>\n   { BEGIN 0; fprintf (stderr, "Invalid string.\n"); /* exit(1); */}

<INQUOTE>.    { *s++ = *yytext; }

\n            { }
.             { }

%%

int yywrap() {
   return 1;
}

int main() {
   yy_flex_debug = 0;
   yylex();
   return 0;
}
