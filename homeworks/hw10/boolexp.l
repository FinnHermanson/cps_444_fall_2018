/******************************************************************************
/
/      filename:  boolexp.l
/
/   description:  
/
/        author:  Finn Hermanson and Kevin McGrail
/     log in id:  FA_18_CPS444_13, FA_18_CPS444_14
/
/         class:  CPS 444
/    instructor:  Perugini
/    assignment:  Homework #10
/
/      assigned:  December 4, 2018
/           due:  December 13, 2018
/
*****************************************************************************/
%{
#include "boolexp.h"
#include "boolexp.tab.h"
%}

%option yylineno

%x LIST 

%%
"["                   { BEGIN LIST;
 						      //printf("%s bracket", yytext);
                        return *yytext;
                      }

<LIST>[a-eg-su-z]     { yylval.environI = *yytext;
 								//printf("%s variable", yytext);
                        return tvar;
                      }
<LIST>[,]        		 { //printf("%s op", yytext); 
								return *yytext; }

<LIST>"]"             { BEGIN 0;
                      //printf("%s bracket", yytext);
                      return *yytext;}

[a-eg-su-z]     		 { yylval.environI = *yytext;
                  		//printf("%s variable", yytext);
                  		return var;
                		 }

[t]             		 { yylval.literal = 1;
 						 	//	printf("%s t_t", yytext);
                   		return literal;
                		 }

[f]             		 { yylval.literal = 0;
 						 	//	printf("%s f_f", yytext);
                   		return literal;
                		 }

[~|&,()]        		 { //printf("%s op", yytext); 
								return *yytext; }

<LIST>[ \t\n]   		 {}
<LIST>.         		 {}

[ \t\n]         		 {}

. 							 {}

%%

int yywrap (void) {
   return 1;
}
