/******************************************************************************
/
/      filename:  compiler.c
/
/   description:  Compiler program that produces an executable that takes
/                 input and determines the boolean expression value until
/                 the EOF character is given.
/                 
/
/        author:  Finn Hermanson and Kevin McGrail
/     log in id:  FA_18_CPS444_13, FA_18_CPS444_14
/
/         class:  CPS 444
/    instructor:  Perugini
/    assignment:  Homework #10
/
/      assigned:  December 4, 2018
/           due:  December 13, 2018
/
*****************************************************************************/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include "boolexp.h"
#include "boolexp.tab.h"

char str[100];
int tmp1;
int tmp2;
int tmp3;
int tmp4;
int tmp5;
int tmp6;
int tmp7;
int tmp8;
int tmp9;
char filecount_str[2];
int filecount = 1;
FILE* file;
int start = 0;
char filename[10];
char truevars[10];
char falsevars[10];
int tcount = 0;
int fcount = 0;

int dummyDFS(PTnode* nodePtr){
   int i = 0;
   i = dfs(nodePtr);
   if ( i == 0) {
     //printf("%s is false.\n", str);
   }
   else {
     // printf("%s is true.\n", str);
   }
   start = 0;
   buildFile(file);
   filecount++;
   memset(str, 0, sizeof str);
}

int dfs(PTnode* nodePtr) {

   if (start == 0) {
      sprintf(filecount_str, "%d", filecount);
      strcpy(filename, filecount_str);
      strcat(filename, ".cpp");
      remove(filename);
      file = fopen(filename,"w");
      fputs("\n", file);
      fputs("#include<iostream>\n\n", file);
      fputs("using namespace std;\n\n", file);
      fputs("main() {\n\n", file);

      start = 1;
   }

   if (!nodePtr){
      printf("test");
      return 0;
   }

   switch (nodePtr->flag) {

      case literalFlag:
         
         tmp4 = nodePtr->literalOrVariable;
         //printf("Literalval: %d\n", tmp4);     
         if (tmp4 == 1) {
             strcat(str,"true");
         }
         else if (tmp4 == 0) {
            strcat(str,"false");
         }
         return tmp4;

      case variableFlag:
         tmp5 = nodePtr->literalOrVariable;
         str[strlen(str)] = tmp5;
         str[strlen(str)] = '\0';
         if (environment[nodePtr->literalOrVariable-'a'] == 1) {
            addTorF(tmp5, 1);
         }
         else {
            addTorF(tmp5, 0);
         }
         return environment[nodePtr->literalOrVariable-'a'];

      case operatorFlag:
          //strcat(str,"(");
          switch (nodePtr->operator1.operatorLiteral) {
             
             case '~':
                strcat(str, "!");
                tmp6 = !dfs(nodePtr->operator1.operands[0]);
            //    strcat(str, ")");
            //    printf("Not: %d \n", tmp6);
                return tmp6;
             
             case '|':
                tmp7 = dfs(nodePtr->operator1.operands[0]);  
              //  printf("OT1: %d \n", tmp1);
                strcat(str, " ");
                strcat(str, "||");
                strcat(str, " ");
                tmp8 = dfs(nodePtr->operator1.operands[1]);  
              //  printf("OT2: %d \n", tmp2);
              //  strcat(str, ")");
                tmp9 = tmp7 || tmp8;
              //  printf("Or: %d \n", tmp9);
                return tmp9;

             case '&':
                tmp1 = dfs(nodePtr->operator1.operands[0]);
                //printf("AT1: %d \n", tmp1);
                strcat(str, " ");
                strcat(str, "&&");
                strcat(str, " ");
                tmp2 = dfs(nodePtr->operator1.operands[1]);
                //printf("AT2: %d \n", tmp2);
                //strcat(str, ")");
                tmp3 = tmp1 && tmp2;
                //printf("And: %d \n", tmp3);
                return tmp3;
      }
   }
   return 0;
}

void addTorF(char letter, int val) {
   if(val == 1) {
      truevars[tcount] = letter;
      tcount++;
   }
   else {
      falsevars[fcount] = letter;
      fcount++;
   }
}

void buildFile(FILE* file) {
   int i = 0;
   
   for(i = 0; i < sizeof(truevars); i++) {
       if(truevars[i] != NULL) {
         fputs("   bool ",file);
         fputc(truevars[i], file);
         fputs(" = true; \n", file);
       }
   }
   if(truevars[0] != NULL){
   fputs("\n", file);}

   for(i = 0; i < sizeof(falsevars); i++) {
      if(falsevars[i]!=NULL){
         fputs("   bool ",file);
         fputc(falsevars[i], file);
         fputs(" = false; \n", file);
      }
   }
   
   if(falsevars[0]!=NULL){
   fputs("\n", file);}

   fputs("   bool result = ", file);
   for( i = 0; i < sizeof(str); i++) {
      if(str[i] != NULL) {
         fputc(str[i], file);
      }
   }
   fputs(";\n\n", file);

   fputs("   cout << \"The result is \";\n\n", file);

   fputs("   if(result)\n", file);
   fputs("      cout << \"true\";\n", file);
   fputs("   else\n", file);
   fputs("      cout << \"false\";\n", file);
   fputs("\n", file);
   fputs("   cout << \".\" << endl;\n", file);
   fputs("}\n", file);

   for(i=0; i<sizeof(truevars); i++) {
      truevars[i] = NULL;
      falsevars[i] = NULL;

   }

   fclose(file);

}
