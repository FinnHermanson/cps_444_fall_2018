/******************************************************************************
/
/      filename:  boolexp.y
/
/   description:  
/
/        author:  Finn Hermanson and Kevin McGrail
/     log in id:  FA_18_CPS444_13, FA_18_CPS444_14
/
/         class:  CPS 444
/    instructor:  Perugini
/    assignment:  Homework #10
/
/      assigned:  December 4, 2018
/           due:  December 13, 2018
/
*****************************************************************************/
%{
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include "boolexp.h"
#define SIZE 26

PTnode* newOperatorNode(int oper, int nops, ...);
PTnode* newLiteralOrVariableNode(int literalOrVariable, PTnodeFlag flag);
void freePTnode(PTnode* nodePtr);
int dummyDFS(PTnode* nodePtr);

void yyerror(char* s);
int environment[SIZE];
extern int yylineno;

%}

%union {
   int literal;
   char environI;
   PTnode* nodePtr;
};

%token <literal> literal
%token <environI> var tvar

%type <nodePtr> program statement sentence declarations varlist expr

%left '|'
%left '&'
%left '~'

%%

program: statement        { exit(0); }
         ;

statement: statement sentence       {  dummyDFS($2); freePTnode($2); }
         | /* NULL */

sentence: '(' declarations ',' expr ')' {$$ = $4; }
         ;

declarations:'['varlist']'   { $$ = $2; }
      |  '[' ']'     {  }
      ;

varlist: tvar   {  PTnode* tnode  = newLiteralOrVariableNode($1, variableFlag); 
                     environment[tnode->literalOrVariable-'a'] = 1;
                     $$ = tnode; }
      |  varlist ',' tvar   {PTnode* tnode = newLiteralOrVariableNode($3, variableFlag);
                     environment[tnode->literalOrVariable-'a'] = 1;
                     $$ = tnode;}
                  /* $$ = newLiteralOrVariableNode($1, variableFlag); }*/
      ;

expr:  '~' expr    {$$ = newOperatorNode('~', 1, $2); }
    | expr '&' expr  { $$ = newOperatorNode('&', 2, $1, $3); }
    | expr '|' expr  { $$ = newOperatorNode('|', 2, $1, $3); }
    | literal        { $$ = newLiteralOrVariableNode($1, literalFlag); }
    | var            {  PTnode* tnode = newLiteralOrVariableNode($1, variableFlag);
                        if (environment[tnode->literalOrVariable-'a'] != 1) 
                           { environment[tnode->literalOrVariable-'a'] = 0; }
                        $$ = tnode;
                     }
   ;

%%

PTnode* newLiteralOrVariableNode(int literalOrVariable, PTnodeFlag flag) {

   PTnode* nodePtr;
   
   if ((nodePtr = malloc (sizeof (*nodePtr))) == NULL)
      yyerror ("out of memory");
   
   nodePtr->flag = flag;
   nodePtr->literalOrVariable = literalOrVariable;

   return nodePtr;
}

PTnode* newOperatorNode (int operatorLiteral, int numOfOperands, ...) {

   va_list ap;
   PTnode* nodePtr;

   int i;

   if ((nodePtr = malloc (sizeof(*nodePtr))) == NULL)
      yyerror ("out of memory");

   nodePtr->flag = operatorFlag;
   nodePtr->operator1.operatorLiteral = operatorLiteral;
   nodePtr->operator1.numOfOperands = numOfOperands;

   va_start(ap, numOfOperands);

   if (((nodePtr->operator1.operands =
         malloc (numOfOperands * sizeof (PTnode*)))) == NULL)
      yyerror("out of memory");

   for (i = 0; i < numOfOperands; i++)
      nodePtr->operator1.operands[i] = va_arg (ap, PTnode*);
   va_end (ap);

   return nodePtr;
}

void freePTnode(PTnode* nodePtr) {

   int i;

   if (!nodePtr)
      return;

   if (nodePtr->flag == operatorFlag) {

      for (i = 0; i < nodePtr->operator1.numOfOperands; i++)
         freePTnode(nodePtr->operator1.operands[i]);
      
      if (nodePtr->operator1.operands)
         free(nodePtr->operator1.operands);
   }
   
   free(nodePtr);
}

void yyerror(char* s) {
   fprintf(stderr, "line %d: %s\n", yylineno, s);
}

int main(void) {
   yyparse();
   return 0;
}
