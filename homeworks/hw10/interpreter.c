/******************************************************************************
/
/      filename:  interpreter.c
/
/   description:  Interpreter program that produces an executable that takes
/                 input and determines the boolean expression value until
/                 the EOF character is given.
/                 
/
/        author:  Finn Hermanson and Kevin McGrail
/     log in id:  FA_18_CPS444_13, FA_18_CPS444_14
/
/         class:  CPS 444
/    instructor:  Perugini
/    assignment:  Homework #10
/
/      assigned:  December 4, 2018
/           due:  December 13, 2018
/
*****************************************************************************/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include "boolexp.h"
#include "boolexp.tab.h"

char str[100];
int tmp1;
int tmp2;
int tmp3;
int tmp4;
int tmp5;
int tmp6;
int tmp7;
int tmp8;
int tmp9;

int dummyDFS(PTnode* nodePtr){
   int i = 0;
   i = dfs(nodePtr);
   if ( i == 0) {
      printf("%s is false.\n", str);
   }
   else {
      printf("%s is true.\n", str);
   }
   memset(str, 0, sizeof str);
}

int dfs(PTnode* nodePtr) {
   if (!nodePtr){
      printf("test");
      return 0;
   }
   switch (nodePtr->flag) {

      case literalFlag:
         
         tmp4 = nodePtr->literalOrVariable;
         //printf("Literalval: %d\n", tmp4);     
         if (tmp4 == 1) {
             strcat(str,"t");
         }
         else if (tmp4 == 0) {
            strcat(str,"f");
         }
         return tmp4;

      case variableFlag:
         tmp5 = nodePtr->literalOrVariable;
         str[strlen(str)] = tmp5;
         str[strlen(str)] = '\0';
         return environment[nodePtr->literalOrVariable-'a'];

      case operatorFlag:
          strcat(str,"(");
          switch (nodePtr->operator1.operatorLiteral) {
             
             case '~':
                strcat(str, "~");
                tmp6 = !dfs(nodePtr->operator1.operands[0]);
                strcat(str, ")");
            //    printf("Not: %d \n", tmp6);
                return tmp6;
             
             case '|':
                tmp7 = dfs(nodePtr->operator1.operands[0]);  
              //  printf("OT1: %d \n", tmp1);
                strcat(str, " ");
                strcat(str, "|");
                strcat(str, " ");
                tmp8 = dfs(nodePtr->operator1.operands[1]);  
              //  printf("OT2: %d \n", tmp2);
                strcat(str, ")");
                tmp9 = tmp7 || tmp8;
              //  printf("Or: %d \n", tmp9);
                return tmp9;

             case '&':
                tmp1 = dfs(nodePtr->operator1.operands[0]);
                //printf("AT1: %d \n", tmp1);
                strcat(str, " ");
                strcat(str, "&");
                strcat(str, " ");
                tmp2 = dfs(nodePtr->operator1.operands[1]);
                //printf("AT2: %d \n", tmp2);
                strcat(str, ")");
                tmp3 = tmp1 && tmp2;
                //printf("And: %d \n", tmp3);
                return tmp3;
      }
   }
   return 0;
}
