/******************************************************************************
/
/      filename:  showheaders.l
/
/   description:  A lexical analyzer that generates a program that finds the
/                 commented and uncommented include filename headers in C/C++
/                 programs
/
/
/        author:  Finn Hermanson
/     log in id:  FA_18_CPS444_13
/
/         class:  CPS 444
/    instructor:  Perugini
/    assignment:  Homework #9
/
/      assigned:  November 20, 2018
/           due:  November 26, 2018
/
/*****************************************************************************/
%{
char *commented[100];
char *uncommented[100];
int un_val = 0;
int com_val = 0;
%}

%%
"#include "[<"][^>"\n]*[>"]           {uncommented[un_val]= strdup(yytext);
                                      un_val++;}
"#include"[<"][^>"\n]*[>"]            {uncommented[un_val] = strdup(yytext);
                                       un_val++;}
^"//".*"#include"[<"][^>"\n]*[>"]     {commented[com_val] = strdup(yytext);
                                       com_val++;}
^"//".*"#include "[<"][^>"\n]*[>"]    {commented[com_val] = strdup(yytext);
                                       com_val++;}
[/][*].*"#include"[<"][^>"\n]*[>"].*[*][/\n]  {commented[com_val] = 
                                               strdup(yytext);
                                               com_val++;}
[/][*].*"#include "[<"][^>"\n]*[>"].*[*][/\n] {commented[com_val] = 
                                               strdup(yytext);
                                               com_val++;}
.|\n                                { /* Ignore other characters */}
%%

int yywrap (void) {
   return 1;
}

int main (int argc, char** argv) {
   int c_vis = 0, u_vis = 0;
   int opt;
   int k,l,i = 0, j = 0;
   while((opt = getopt(argc, argv,":cu")) != -1){  
      switch(opt){  
         case 'c':
            c_vis = 1;
            break;
         case 'u':
            u_vis = 1;
            break;
         case '?':
            printf("./showheaders: Illegal option: [-%c]\n", optopt);
            printf("Usage: ./showheaders [-cu] [files ...]");
            return(1);
      }
   }
   if (optind == 1){
      c_vis = 1;
      u_vis = 1;
   }
   if (optind == argc){
      yyin = stdin;
      yylex();
   } 
   else {
      for (; optind < argc; optind++) {
         if ((yyin = fopen(argv[optind], "r")) == NULL){
            printf("./showheaders: Invalid File: %s\n", argv[optind]);
            printf("Usage: ./showheaders [-cu] [files ...]");
            return(2);
         } 
         yylex();
      }
   }
   if (u_vis == 1){
      printf("Uncommented header file(s):\n");
      for (i = 0; i < un_val; i++){
         printf("%s\n", uncommented[i]);
      }
   }
   if (c_vis == 1){
      printf("Commented header file(s):\n");
      for (j = 0; j < com_val; j++){
        printf("%s\n", commented[j]);
      }
   }
   fclose(yyin);
   return 0;
}
