#include <stdio.h>
#include "calc3.h"
#include "calc3.tab.h"

static int lbl;

/* does a depth-first traversal; therefore visits nodes in the
   order that they were allocated during parsing */
int dfs(PTnode* nodePtr) {

   int lbl1, lbl2;

   if (!nodePtr) return 0;

   switch (nodePtr->flag) {

      case literalFlag:

         printf ("\tpush\t%d\n", nodePtr->literalOrVariable);
         break;

      case variableFlag:

         printf ("\tpush\t%c\n", nodePtr->literalOrVariable + 'a');
         break;

      case operatorFlag:

         switch (nodePtr->operator1.operatorLiteral) {

            case WHILE:

               printf ("L%03d:\n", lbl1 = lbl++);
               dfs (nodePtr->operator1.operands[0]);
               printf ("\tjz\tL%03d\n", lbl2 = lbl++);
               dfs (nodePtr->operator1.operands[1]);
               printf ("\tjmp\tL%03d\n", lbl1);
               printf ("L%03d:\n", lbl2);
               break;

            case IF:

               dfs (nodePtr->operator1.operands[0]);

               if (nodePtr->operator1.numOfOperands > 2) {

                  /* if else */
                  printf ("\tjz\tL%03d\n", lbl1 = lbl++);
                  dfs (nodePtr->operator1.operands[1]);
                  printf ("\tjmp\tL%03d\n", lbl2 = lbl++);
                  printf ("L%03d:\n", lbl1);
                  dfs (nodePtr->operator1.operands[2]);
                  printf ("L%03d:\n", lbl2);

               } else {

                    /* if */
                    printf ("\tjz\tL%03d\n", lbl1 = lbl++);
                    dfs (nodePtr->operator1.operands[1]);
                    printf ("L%03d:\n", lbl1);
                 }

               break;

            case PRINT:

               dfs (nodePtr->operator1.operands[0]);
               printf ("\tprint\n");
               break;

            case '=':

               dfs (nodePtr->operator1.operands[1]);
               printf ("\tpop\t%c\n", nodePtr->operator1.operands[0]->literalOrVariable + 'a');
               break;

            case UMINUS:

               dfs (nodePtr->operator1.operands[0]);
               printf ("\tneg\n");
               break;

            default:

               dfs (nodePtr->operator1.operands[0]);
               dfs (nodePtr->operator1.operands[1]);

               switch (nodePtr->operator1.operatorLiteral) {

                  case '+':
                     printf ("\tadd\n");
                     break;

                  case '-':
                     printf ("\tsub\n");
                     break;

                  case '*':
                     printf ("\tmul\n");
                     break;

                  case '/':
                     printf ("\tdiv\n");
                     break;

                  case '^':
                     printf ("\texp\n");
                     break;

                  case '<':
                     printf ("\tcompLT\n");
                     break;

                  case '>':
                     printf ("\tcompGT\n");
                     break;

                  case GE:
                     printf ("\tcompGE\n");
                     break;

                  case LE:
                     printf ("\tcompLE\n");
                     break;

                  case NE:
                     printf ("\tcompNE\n");
                     break;

                  case EQ:
                     printf ("\tcompEQ\n");
               }
         }
   }
}
