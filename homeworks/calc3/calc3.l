%{
#include "calc3.h"
#include "calc3.tab.h"
%}

%option yylineno

%%

[a-z]   { /* variables */
          yylval.environI = *yytext - 'a';
          return VARIABLE; }

0   { yylval.literal = 0;
      return INTEGER; }

[1-9][0-9]* { /* integers */
              yylval.literal = atoi (yytext);
              return INTEGER; }

[-^()<>=+*/;{}]   { /* single-character operators returned as themself */
                   return *yytext; }

">="    { /* other operators returned as tokens */
          return GE; }

"<="   return LE;
"=="   return EQ;
"!="   return NE;
"while"   return WHILE;
"if"   return IF;
"else"   return ELSE;
"print"   return PRINT;

[ \t\n]   { /* ignore whitespace */ ; }

.   yyerror ("Unknown character");

%%

int yywrap (void) {
   return 1;
}
