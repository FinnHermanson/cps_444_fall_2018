/******************************************************************************
/
/      filename:  env1.go 
/
/   description:  Go version of the Linux command env
/
/        author:  Hermanson, Finn
/      login id:  FA_18_CPS444_13
/
/         class:  CPS 444
/    instructor:  Perugini
/    assignment:  Homework #2
/
/      assigned:  August 30, 2018
/           due:  September 6, 2018
/
/*****************************************************************************/
package main

import (
   "fmt"
   "flag"
   "os/exec"
   "strings"
   "syscall"
   "os"
)


func main(){
   iPtr := flag.Bool("i", false, "ignore current environment")
   flag.Parse()

   var env []string
   cmdArgs := flag.Args()
   var execArgs []string   

   envVars := false
   envPath := "env"

   for j := 0; j < len(cmdArgs); j++ {
      if envVars {
         execArgs = append(execArgs, cmdArgs[j])
      }
      if strings.Compare(cmdArgs[j], "./env1") == 0 {
         envVars = true
      }
      if strings.Compare(cmdArgs[j], "ls") == 0 {
         envP, lookErr := exec.LookPath("ls")
         if lookErr != nil {
            fmt.Fprintln(os.Stderr, "error getting utility")
         } 
         envPath = envP     
      }

   }

   if *iPtr {
      for i := 0; i < len(cmdArgs); i++ {
         if strings.Contains(cmdArgs[i], "=") {
            env = append(env, cmdArgs[i])
         } 
      } 
      envP, lookErr := exec.LookPath("env")
      if lookErr != nil {
         fmt.Fprintln(os.Stderr, "error getting utility")
      }
      envPath = envP
   } else {
      env := os.Environ()
      for i := 0; i < len(cmdArgs); i++ {
         if strings.Contains(cmdArgs[i], "=") {
            env = append(env, cmdArgs[i])
         }   
      }
      envP, lookErr := exec.LookPath("ls")
      if lookErr != nil {
         fmt.Fprintln(os.Stderr, "error getting utility")
      }
      envPath = envP
   }

   execErr := syscall.Exec(envPath, execArgs, env)
   if execErr != nil {
      fmt.Fprintln(os.Stderr, "no system call")
   }
   

} 
