%{
#include "calc1.tab.h"
/*
#define YYSTYPE int
extern YYSTYPE yylval;
*/
%}

%%

0	{
	   /* get integer value of INTEGER token */
      yylval = atoi (yytext);
      return INTEGER;
	}

[1-9][0-9]*	{
	   /* get integer value of INTEGER token */
      yylval = atoi (yytext);
      return INTEGER;
	}

[-+\n]	{ return *yytext; }

[ \t]	;  /* skip whitespace */

.         yyerror ("invalid character");

%%

int yywrap (void) {
   return 1;
}
