/* token values typically start around 258
   because values 0-255 are reserved for character values and
   lex reserves several values for end-of-file and error processing
 */

/* produces "#define INTEGER 258" in y.tab.c on our system */
%token INTEGER

%{
#include<stdio.h>
#define YYDEBUG 0
%}

%left '+' '-'

%%

program: program expr '\n'	{ printf ("%d\n", $2); }
	 | expr '\n'            {printf ("%d\n", $1);}
         ;

expr: INTEGER			{ $$ = $1; /* default action: pop, push */ }

      | expr '+' expr 		{
				    /* addition */
				    $$ = $1 + $3;
				}

      | expr '-' expr		{
				    /* subtraction */
				    $$ = $1 - $3;
				}
      ;

%%

int yyerror (char* s) {
   fprintf (stderr, "%s\n", s);
   return 0;
}

int main (void) {
#if YYDEBUG
   yydebug = 0;
//   yy_flex_debug = 1;
#endif
   yyparse();
   return 0;
}
