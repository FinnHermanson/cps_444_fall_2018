/*******************************************************************************
/  Grade: 38/60 
/
/  *  ClearLog correct, Style correct, and close on most functions but does not run correctly
/  
/
/
/
/ 
/
/******************************************************************************/

/******************************************************************************
/
/      filename:  loggerlib.go
/
/   description:  A Go library that logs commands and input. The library
/                 creates a log that points to each next input in order to 
/                 access data.
/
/        author:  Hermanson, Finn
/      login id:  FA_18_CPS444_13
/
/         class:  CPS 444
/    instructor:  Perugini
/    assignment:  Homework #3
/
/      assigned:  September 6, 2018
/           due:  September 13, 2018
/
/*****************************************************************************/

package loggerlib

import (
   "time"
   "errors"
   "strings"
   "os"
   "io/ioutil")

type Data_t struct {
   Logged_time time.Time
   Str string
}

type Log_t struct {
   item Data_t
   next *Log_t
}

// global, private variables
var headptr *Log_t
var tailptr *Log_t

func Addmsg (data Data_t) (int, error) {
   newLog := Log_t{data, nil}
   if headptr==nil {
      headptr = &newLog
      tailptr = &newLog
   } else if headptr == tailptr {
      tailptr = &newLog
      headptr.next = &newLog
   } else {
      tailptr.next = &newLog
      tailptr = &newLog
   }
   
   if headptr!=nil{
      return 0, nil
   } else {
      return -1, errors.New("Message could not be added")
   }

}

func Clearlog() {
   headptr = nil
   tailptr = nil
}

func Getlog() (string, error) {
   var curLog *Log_t
   curLog = headptr
   var message string
   var timestr string
   returnstr := ""
   for curLog.next!=nil {
      timestr = formatTime(curLog.item)
      message = curLog.item.Str
      returnstr += "Time: " + timestr + "\n"
      returnstr += "Message: " + message + "\n\n"
      curLog = curLog.next
   }
   timestr = formatTime(curLog.item)
   message = curLog.item.Str
   returnstr += "Time: " + timestr + "\n"
   returnstr += "Message: " + message + "\n\n"
   
   if len(returnstr) != 0 {
      return returnstr, nil
   } else {
      return "", errors.New("Error using GetLog")
   }
}

func Savelog (filename string) error {
   var curLog *Log_t 
   curLog= headptr
   var timestr string
   var message string
   writeMes := ""
   _, err := os.Stat(filename)
   if err == nil {
      
      for curLog.next!=nil {
         timestr = formatTime(curLog.item)
         message = curLog.item.Str 
         writeMes += "Time: " + timestr + 
                     "\nMessage: " + message + "\n\n"    
      }
      timestr = formatTime(curLog.item)
      message = curLog.item.Str
      writeMes += "Time: " + timestr + "\n"
      writeMes += "Message: " + message + "\n\n"
      
      writeMesByte := []byte(writeMes)
      ioutil.WriteFile(filename, writeMesByte, 0)
      return nil
   } else {
      return err
   }     
}

func formatTime(data Data_t) string {
   var timestr string
   var months map[string]string

   months = make(map[string]string)

   months["Jan"] = "01"; months["Feb"] = "02"; months["Mar"] = "03"
   months["Apr"] = "04"; months["May"] = "05"; months["Jun"] = "06"
   months["Jul"] = "07"; months["Aug"] = "08"; months["Sep"] = "09"
   months["Oct"] = "10"; months["Nov"] = "11"; months["Dec"] = "12"

   time_to_format := data.Logged_time
   const layout = "Jan 2 2006 15:04:05"
   timeslice := strings.Split(time_to_format.Format(layout), " ")
   timestr = months[timeslice[0]] + "/"
   
   if len(timeslice[1]) == 1 {
      timestr += "0"
   }
   
   timestr += timeslice[1] + "/" + timeslice[2] + " " + timeslice[3]
   
   return timestr
}
