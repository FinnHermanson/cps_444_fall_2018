/******************************************************************************
/
/      filename:  diff1.go
/
/   description:  Version of the Linux file comparison program diff in Go.
/
/
/        author:  Finn Hermanson
/     log in id:  FA_18_CPS444_13
/
/         class:  CPS 356
/    instructor:  Perugini
/    assignment:  Homework #1
/
/		 assigned:  August 23, 2018
/           due:  August 30, 2018
/
/*****************************************************************************/
package main

import (
   "fmt"
   "flag"
   "bufio"
   "os"
   "io"
   "strings"
)

   func diff(one string, two string, a bool, l bool, t bool, m bool) bool {
      var comp1, comp2 string
      if a {
         comp1 = strings.Replace(one, " ", "", -1)
         comp2 = strings.Replace(two, " ", "", -1)              
         one = comp1
         two = comp2
      }
      if l {
         comp1 = strings.TrimLeft(one, " ")
         comp2 = strings.TrimLeft(two, " ")     
         one = comp1
         two = comp2
      }
      if t {
         comp1 = strings.TrimRight(one, " ")
         comp2 = strings.TrimRight(two, " ")
         one = comp1
         two = comp2
      }
      if m {
         noOutside1 := strings.TrimSpace(one)
         noOutside2 := strings.TrimSpace(two)
         
         noSpace1 := strings.Replace(one, " ", "", -1)
         noSpace2 := strings.Replace(two, " ", "", -1)
         
         comp1 = strings.Replace(one, noOutside1, noSpace1, -1)
         comp2 = strings.Replace(two, noOutside2, noSpace2, -1)
         
         one = comp1
         two = comp2
      }
            
      if one != two {
         return true
      } else {
         return false
      }
   }

   func main(){
      lPtr := flag.Bool("l", false, "ignore leading whitespace")
      tPtr := flag.Bool("t", false, "ignore trailing whitespace")
      mPtr := flag.Bool("m", false, "ignore middle whitespace")
      aPtr := flag.Bool("a", false, "ignore all whitespace")
      count := 1
      flag.Parse()

      if *aPtr && (*lPtr || *tPtr || *mPtr){
         fmt.Fprintf(os.Stderr,
               "Option -a cannot be combined with any other options.\n")
         os.Exit(9)
      }
       
      if flag.Arg(0) != "-" && flag.Arg(1) != "-" {
         file1, err := os.Open(flag.Arg(0))
         if err != nil {
            fmt.Fprintf(os.Stderr, "error reading file")
            os.Exit(2)
         }
         
         file2, err := os.Open(flag.Arg(1))
         if err != nil {
            fmt.Fprintf(os.Stderr, "error reading file")
            os.Exit(2)
         }
			scanner := bufio.NewScanner(file1)
         scanner2 := bufio.NewScanner(file2)
         
         scannerNextLine := false
         scanner2NextLine := false
         
         if scanner.Scan() {
            scannerNextLine = true
         }
         if scanner2.Scan() {
            scanner2NextLine = true
         }
          
         for scannerNextLine || scanner2NextLine {
            if scannerNextLine && scanner2NextLine{
               line1 := scanner.Text()
               line2 := scanner2.Text()
               if diff(line1, line2, *aPtr, *lPtr, *tPtr, *mPtr){
                  fmt.Printf("%d\n", count)
               }
            } else if scannerNextLine && !scanner2NextLine {
               line1 := scanner.Text()
               line2 := ""
               if diff(line1, line2, *aPtr, *lPtr, *tPtr, *mPtr){
                  fmt.Printf("%d\n", count)
               }
            } else if !scannerNextLine && scanner2NextLine {
               line1 := ""
               line2 := scanner2.Text()
               if diff(line1, line2, *aPtr, *lPtr, *tPtr, *mPtr){
                  fmt.Printf("%d\n", count)
               }
            }

            count++
                     
            if scanner.Scan() {
               scannerNextLine = true
            } else {
               scannerNextLine = false
            }
            if scanner2.Scan() {
               scanner2NextLine = true
            } else {
               scanner2NextLine = false
            }         
            //fmt.Printf("file 1: %s\n", scanner.Text())
            //fmt.Printf("file 2: %s\n", scanner2.Text())
         }
      }

		if flag.Arg(0) == "-" && flag.Arg(1) != "-" {
         file, err := os.Open(flag.Arg(1))
         if err != nil {
            fmt.Fprintf(os.Stderr, "error reading file")
            os.Exit(2)
         }
         scanner := bufio.NewScanner(file)
         
         reader := bufio.NewReader(os.Stdin)
         input, err1 := reader.ReadString('\n')

         scannerNextLine := false
         if scanner.Scan(){
            scannerNextLine = true
         }
         
         for err1 != io.EOF || scannerNextLine{
            if scannerNextLine{
               line := scanner.Text()
               if diff(input, line,  *aPtr, *lPtr, *tPtr, *mPtr){
                  fmt.Printf("%d\n", count)
               }
            
            } else {
               line := scanner.Text()
               if diff(input, line,  *aPtr, *lPtr, *tPtr, *mPtr){
                  fmt.Printf("%d\n", count)
               }
            }
            count++
            if scanner.Scan() {
               scannerNextLine = true
            } else {
               scannerNextLine = false
            }
            
         }
      }
	
      if flag.Arg(1) == "-" && flag.Arg(0) != "-" {
         file, err := os.Open(flag.Arg(0))
         if err != nil {
            fmt.Fprintf(os.Stderr, "error reading file")
            os.Exit(2)
         }
         scanner := bufio.NewScanner(file)
         
         reader := bufio.NewReader(os.Stdin)
         input, err1 := reader.ReadString('\n')

         scannerNextLine := false
         if scanner.Scan() {
            scannerNextLine = true
         }
         
         for err1 != io.EOF || scannerNextLine{
            if scannerNextLine {
               line := scanner.Text()
               if diff(input, line,  *aPtr, *lPtr, *tPtr, *mPtr){
                  fmt.Printf("%d\n", count)
               }
            } else {
               line := ""
               if diff(input, line,  *aPtr, *lPtr, *tPtr, *mPtr){
                  fmt.Printf("%d\n", count)
               }
            }
            input, err1 = reader.ReadString('\n')
            count++
            if scanner.Scan() {
               scannerNextLine = true
            } else {
               scannerNextLine = false
            }
         }
      }

      if flag.Arg(0) == "-" && flag.Arg(1) == "-" {
         reader := bufio.NewReader(os.Stdin)
         input, err := reader.ReadString('\n')

         for err != io.EOF {
            if diff(input, input, *aPtr, *lPtr, *tPtr, *mPtr) {
               fmt.Printf("%d\n", count)
            }
            input, err = reader.ReadString('\n')
         }
      } 
}
