package main

import ( 
 "fmt"
 "strings"
)

func mod(x int, a bool){
   if a{
     x = x + 1}
   fmt.Printf("%d",x)
}

func main() {
  test := "   hey there boy"
  result := strings.TrimLeft(test, " ")
  fmt.Println(result)
  
  hella := true
  num := 32
  mod(num, hella)
}
