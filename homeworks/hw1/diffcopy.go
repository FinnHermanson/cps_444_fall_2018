/******************************************************************************
/
/      filename:  diff1.go
/
/   description:  Version of the Linux file comparison program diff in Go.
/
/
/        author:  Finn Hermanson
/     log in id:  FA_18_CPS444_13
/
/         class:  CPS 356
/    instructor:  Perugini
/    assignment:  Homework #1
/
/		 assigned:  August 23, 2018
/           due:  August 30, 2018
/
/*****************************************************************************/
package main

import (
   "fmt"
   "flag"
   "bufio"
   "os"
)
   func main(){
      lPtr := flag.Bool("l", false, "ignore leading whitespace")
      tPtr := flag.Bool("t", false, "ignore trailing whitespace")
      mPtr := flag.Bool("m", false, "ignore middle whitespace")
      aPtr := flag.Bool("a", false, "ignore all whitespace")
      
      flag.Parse()

      if flag.Arg(0) != "-" && flag.Arg(1) != "-" {
         file1, err := os.Open(flag.Arg(0))
         if err != nil {
            fmt.Fprintf(os.Stderr, "error reading file")}
         if err != nil {
            fmt.Fprintf(os.Stderr, "error reading file")}
         file2, err := os.Open(flag.Arg(1))
			scanner := bufio.NewScanner(file1)
         scanner2 := bufio.NewScanner(file2)
         for scanner.Scan() && scanner2.Scan() {
            if *aPtr && (*lPtr || *tPtr || *mPtr){
               fmt.Fprintf(os.Stderr,
                        "Option -a cannot be combined with any other options.\n")
               os.Exit(9)
            }
            fmt.Printf("file 1: %s\n", scanner.Text())
            fmt.Printf("file 2: %s\n", scanner2.Text())
         }
			
      }

      
      fmt.Printf("other args: %+v\n", flag.Args())
}
