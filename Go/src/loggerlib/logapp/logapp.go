package main

// THIS FILE MUST NOT BE MODIFIED

import (
   "io"
   "os"
   "fmt"
   "bufio"
   "strings"
   "loggerlib/logapp/logapp_helperfuns")

func main() {
   var cmd string
   var err error
   showhistory := true
   const HISTFILE = "history.log"
   argc := len(os.Args)

   if argc == 1 {
      showhistory = false
   } else if argc > 2 || os.Args[1] != "history" {
         fmt.Fprintf (os.Stderr, "Usage: %s [history]\n", os.Args[0])
         os.Exit(1)
   }   

   stdin := bufio.NewReader(os.Stdin)

   cmd, err = stdin.ReadString('\n')

   for err != io.EOF {
      cmd = strings.TrimSuffix(cmd, "\n")
      if showhistory && (cmd == "showhistory") {
         logapp_helperfuns.Displayhist()
      } else {
           _,err = logapp_helperfuns.Execmd(cmd)
           if err != nil {
              panic(err)
           }
        }
      cmd, err = stdin.ReadString('\n')
   }

   fmt.Printf ("\n\n~~~~~~ The following is the list of commands ordered by\n")
   fmt.Printf ("~~~~~~ the time at which each was executed:\n")

   logapp_helperfuns.Displayhist()

   logapp_helperfuns.Savehistory(HISTFILE)

   logapp_helperfuns.Clearhistory()

   os.Exit(0) 
}
