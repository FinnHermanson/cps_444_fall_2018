package main

import (
   "os"
   "strconv"
   "hw6/barrierclient/waitatbarrier"
)

func main() {

   i,_ := strconv.Atoi(os.Args[1])

   /* first barrier */
   waitatbarrier.Waitatbarrier("barrier", i, 1)

   /* second barrier */
   waitatbarrier.Waitatbarrier("barrier", i, 2)

   os.Exit(0)
}
