/*******************************************************************************
/
/      filename:  waitatbarrier.go
/
/   description:  Manages the communcation between client and server at a 
/						barrier.
/
/        author:  Hermanson, Finn
/      login id:  FA_18_CPS444_13
/
/         class:  CPS 444
/    instructor:  Perugini
/    assignment:  Homework #6
/
/      assigned:  October 11, 2018
/           due:  October 18, 2018
/
/******************************************************************************/

package waitatbarrier

import(
   "os"
   "syscall"
   "fmt"
)

func Waitatbarrier(name string, i int, bid int) error {
   const request = uint32(syscall.S_IRUSR | syscall.S_IWUSR | 
                          syscall.S_IWGRP | syscall.S_IWOTH)
   const release = uint32(syscall.S_IWUSR | syscall.S_IRUSR | 
                          syscall.S_IRGRP | syscall.S_IROTH)

   reqPath :=  name + ".request"
   relPath :=  name + ".release"
   
   wrByte := make([]byte, 1)
	wrByte[0] = 'g'
   for k := 1; k <= bid; k++{
      fmt.Fprintln(os.Stderr, "i:", i, "pid:", os.Getpid(), 
                           ", ppid:", os.Getppid(), "just arrived at ", 
                           name, k)

      reByte := make([]byte, 1)
      bytesRead := 1

      //writing
      requestfd, openErr := syscall.Open(reqPath, syscall.O_WRONLY, request)
      if (openErr != nil){
         return openErr
      }

      _, writeErr := syscall.Write(requestfd, wrByte)
      if (writeErr != nil){
         return writeErr
      }
      fmt.Fprintln(os.Stderr, "client", i, "just wrote g in 1 byte to request pipe")
      
      closeErr := syscall.Close(requestfd)
      if (closeErr != nil){
         return closeErr
      }


      //reading
      releasefd, openErr := syscall.Open(relPath, syscall.O_RDONLY, release)
      if (openErr != nil){
         return openErr
      }
      
      
      for j := 0; j < 1; {
         bytesRead, readErr := syscall.Read(releasefd, reByte)
         if (bytesRead == 1){
            j++
         }
         if (readErr != nil){
            return readErr
         }
      }
      fmt.Fprintln(os.Stderr, "i:", i, ", pid:", os.Getpid(), ", ppid:", 
                       os.Getppid(), ", just read g in ", bytesRead,
                       " byte from release pipe",name, k)
      
      closeErr = syscall.Close(releasefd)
      if (closeErr != nil){
         return closeErr
      }
      fmt.Fprintln(os.Stderr, "i:", i, ", pid:",os.Getpid(), 
                     ", ppid:", os.Getppid(), ", just passed", 
                     name, k)
   }
   return nil
}
