/*******************************************************************************
/
/      filename:  barrierserver.go
/
/   description:  Creates and manages the request and release pipes that are
/                 used by the client and server to indicate how many processes
/                 run and send data to the barrier. Runs infinitely
/
/        author:  Hermanson, Finn
/      login id:  FA_18_CPS444_13
/
/         class:  CPS 444
/    instructor:  Perugini
/    assignment:  Homework #6
/
/      assigned:  October 11, 2018
/           due:  October 18, 2018
/
/******************************************************************************/

package main

import (
   "strconv"
	"os"
	"syscall"
   "fmt"
)

func main(){
   name := os.Args[1]
   barriersize, strErr := strconv.Atoi(os.Args[2])
   if (strErr != nil) {
      os.Exit(-1)
   }

   reqPath := name + ".request"
   relPath := name + ".release"

	const request = uint32(syscall.S_IRUSR | syscall.S_IWUSR | syscall.S_IWGRP | syscall.S_IWOTH)
   const release = uint32(syscall.S_IWUSR | syscall.S_IRUSR | syscall.S_IRGRP | syscall.S_IROTH)

	reqErr := syscall.Mkfifo(reqPath, request)
   if (reqErr != nil){
      fmt.Fprintln(os.Stderr, "Failed to create request pipe")
      os.Exit(-1)
   } 
	relErr := syscall.Mkfifo(relPath, release)
   if (relErr != nil){
      fmt.Fprintln(os.Stderr, "Failed to create release pipe")
      os.Exit(-1)
   }

   bytes := make([]byte, barriersize)

   for {

      reByte := make([]byte, 1)

      //reading
      requestfd, openErr := syscall.Open(reqPath, syscall.O_RDONLY, request)
      if (openErr != nil){
         fmt.Fprintln(os.Stderr, "Failed to open request pipe")
         os.Exit(-1)
      }

      for k := 0; k < barriersize; {   
         for j := 0; j < 1; {
            bytesRead, readErr := syscall.Read(requestfd, reByte)
            if (bytesRead == 1){
               j++
            }
            if (readErr != nil){
               fmt.Fprintln(os.Stderr, "Failed to read from request pipe")
            }
         }
         bytes[k] = reByte[0]
         k++
         fmt.Fprintln(os.Stderr, "server read ",string(reByte[0])," in 1 byte.")
      }
      closeErr := syscall.Close(requestfd)
      if (closeErr != nil){
         fmt.Fprintln(os.Stderr, "Failed to close request pipe")
      }

      //writing
      releasefd, openErr := syscall.Open(relPath, syscall.O_WRONLY, release)
      if (openErr != nil){
         fmt.Fprintln(os.Stderr, "Failed to open release pipe")
         os.Exit(-1)
      }
      
      _, writeErr := syscall.Write(releasefd, bytes)
      if (writeErr != nil){
         fmt.Fprintln(os.Stderr, "Failed to write to release pipe")
         os.Exit(-1)
      }
      fmt.Fprintln(os.Stderr, "Server wrote", string(bytes[0:5]) ,
                      "in", barriersize, "bytes to release pipe")

      closeErr = syscall.Close(releasefd)
      if (closeErr != nil){
         fmt.Fprintln(os.Stderr, "Failed to close release pipe\n")
      }

      fmt.Fprintln(os.Stderr, "server just closed the release pipe\n")     

   }

}


